package stsg.code.sentence_movement;

import java.util.ArrayList;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import stsg.code.util.Node;
import stsg.code.util.OutputResult;
import stsg.code.util.Rule;
import stsg.code.util.RuleParser;
import stsg.code.util.TreeParser;

public class MovementSTSGImpl implements stsg.code.ISTSG {

	@Override
	public String synchronous_tree_substitution_grammar(String str) {
		// TODO Auto-generated method stub
		java.util.List<Term> sparse = ToAnalysis.parse(str);
		String[] source = new String[sparse.size()]; // 原句分词String[]
		for (int i = 0; i < source.length; i++) {
			source[i] = sparse.get(i).getName();
		}
		Node sroot = StanfordParser.createTree(source); // 原句建树
		// System.out.println("原句子语法树:" + StanfordParser.printTree(sroot));
		// 取到原句建树中的所有结点Node
		TreeParser facility = new TreeParser(sroot);
		ArrayList<Node> allNodes = facility.list;
		// 开始比对规则
		int cnt = 0;
		while (true) {
			boolean canExit = true;
			for (int i = 0; i < allNodes.size() - source.length; i++) {
				// 导入规则
				MakeRulesFromCorpus.rulesTreeList.clear();
				MakeRulesFromCorpus.ImportRules();
				ArrayList<Rule> rlist = MakeRulesFromCorpus.rulesTreeList;
				for (Rule r : rlist) {
					if (RuleParser.isEqualTree(allNodes.get(i), r.source)) {
						facility.Visit(allNodes.get(i), r.source);
						facility.Visit_Target(r.target);
						allNodes.get(i).replace(r.target);
						// System.out.println("匹配到的规则:"
						// + StanfordParser.printTree(r.source));
						canExit = false;
						break;
					}
				}
			}
			if (canExit) {
				break;
			}
			cnt++;
			if (cnt >= 100) {
				System.out.println("Wrong!");
				break;
			}
		}
		// System.out.println("目标句语法树:" + StanfordParser.printTree(sroot));
		OutputResult outans = new OutputResult();
		outans.visit(sroot);
		// System.out.println("移位修正结果:" + outans);
		return outans.toString();
	}

}
